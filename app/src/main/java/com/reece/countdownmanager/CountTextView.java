package com.reece.countdownmanager;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ReeceCheng on 2016/2/15.
 */
public class CountTextView extends TextView implements CountdownObserver {

    private String name;
    private String time;
    private int min, sec, hour;

    private WeakReference<CountdownObserver> weakObserver;

    public CountTextView(Context context) {
        super(context);
        initInternal();
    }

    public CountTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInternal();
    }

    public CountTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInternal();
    }

    public CountTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInternal();
    }

    public void initInternal() {
        weakObserver = new WeakReference<CountdownObserver>(this);
    }

    public void init(String name, String time) {
        this.name = name;
        this.time = time;

        CountdownManager.getInstance().registerObserver(weakObserver);

        long curTime = CountdownManager.getInstance().getCurTime();
        calculateTime(time, curTime);
    }

    private void calculateTime(String time, long curTime) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {

//            Date curDate = new Date(System.currentTimeMillis());
            Date targetDate = sdf.parse(time);

            if(targetDate.getTime() >= curTime) {

                Long second = (targetDate.getTime() - curTime) / 1000;


                hour = (int)(second / 60 / 60);
                min = (int)(second / 60 % 60);
                sec = (int)(second % 60);

//                Log.d("REECE", String.format(name+", "+time+", %02d:%02d:%02d, target: "+targetDate.getTime()+", curTime: "+curTime+", second: "+second, hour, min, sec));
                setText(String.format(name + ", " + time + ", %02d:%02d:%02d", hour, min, sec));
            } else {
                hour = 0;
                min = 0;
                sec = 0;

                CountdownManager.getInstance().removeObserver(weakObserver);
                Log.d("REECE", String.format(name + ", " + time + ", %02d:%02d:%02d, target: " + targetDate.getTime() + ", curTime: " + curTime + ", second: 0 TIMES UP!!", hour, min, sec));
                setText(String.format(name + ", " + time + ", %02d:%02d:%02d\nTIMES UP!!!", hour, min, sec));
            }
        } catch (Exception e) {
            Log.e("REECE", "TargetDate parse FAILED!!");
        }

    }

    @Override
    public void update() {
        if(sec > 0) {
            sec--;

            Log.d("REECE", String.format(name+", "+time+", %02d:%02d:%02d, UPDATED!! ", hour, min, sec));
            setText(String.format(name+", "+time+", %02d:%02d:%02d", hour, min, sec));
        } else if(sec == 0 && min > 0) {
            min--;
            sec = 59;

            Log.d("REECE", String.format(name+", "+time+", %02d:%02d:%02d, UPDATED!! ", hour, min, sec));
            setText(String.format(name+", "+time+", %02d:%02d:%02d", hour, min, sec));
        } else if(sec == 0 && min == 0 && hour > 0) {
            hour--;
            min = 59;
            sec = 59;

            Log.d("REECE", String.format(name+", "+time+", %02d:%02d:%02d, UPDATED!! ", hour, min, sec));
            setText(String.format(name+", "+time+", %02d:%02d:%02d", hour, min, sec));
        } else if(sec == 0 && min == 0 && hour == 0) {
//            Log.d("REECE", name+" TIMES UP!!!");
            CountdownManager.getInstance().removeObserver(weakObserver);
            setText(String.format(name+", "+time + ", %02d:%02d:%02d\nTIMES UP!!!", hour, min, sec));
        }

    }

    @Override
    public String getName() {
        return name;
    }

}
