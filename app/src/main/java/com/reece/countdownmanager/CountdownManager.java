package com.reece.countdownmanager;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ReeceCheng on 2016/2/4.
 */
public class CountdownManager implements Subject{

    private static CountdownManager sInstance;

    private boolean isCountdownRunning;
    private long mCurTime;

    private Timer mTimer;
    private ArrayList<WeakReference<CountdownObserver>> mCountdownObservers;

    public static CountdownManager getInstance() {
        if (sInstance == null) {
            synchronized (CountdownManager.class) {
                if (sInstance == null) {
                    sInstance = new CountdownManager();
                }
            }
        }

        return sInstance;
    }

    public CountdownManager() {
        mCountdownObservers = new ArrayList<>();
        isCountdownRunning = false;

        Date curDate = new Date(System.currentTimeMillis());
        mCurTime = curDate.getTime();
    }

    public long getCurTime() {
        return mCurTime;
    }

    private void startCountdown() {
        isCountdownRunning = true;
//        Log.d("REECE", "Countdown START");

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Date curDate = new Date(System.currentTimeMillis());
                mCurTime = curDate.getTime();

                sendCountdownEvent();
            }
        }, 1000, 1000);
    }

    private void sendCountdownEvent() {
        notifyObservers();
        toggleCountdown();
    }

    public void stopCountdown() {
        Log.d("REECE", "Countdown STOP");
        isCountdownRunning = false;

        if(mTimer != null) {
            mTimer.cancel();
        }
    }

    @Override
    public void registerObserver(WeakReference<CountdownObserver> o) {
        if(mCountdownObservers == null) {
            mCountdownObservers = new ArrayList<>();
        }

        if(!mCountdownObservers.contains(o)) {
            Log.d("REECE", "CountdownManager: "+o.get().getName()+" registerObserver OK!");
            mCountdownObservers.add(o);
        } else {
            Log.e("REECE", "CountdownManager: " + o.get().getName() + " registerObserver ALREADY REGISTER!!");
        }

        toggleCountdown();
    }

    public void toggleCountdown() {
        if(mCountdownObservers != null && !mCountdownObservers.isEmpty() && !isCountdownRunning) {
            startCountdown();
        } else if(mCountdownObservers != null && mCountdownObservers.isEmpty() && isCountdownRunning){
            stopCountdown();
        }
    }

    @Override
    public void removeObserver(WeakReference<CountdownObserver> o) {
        if(mCountdownObservers != null) {
            mCountdownObservers.remove(o);
            Log.d("REECE", "CountdownManager: " + o.get().getName() + " removed!!!");
        }
    }

    @Override
    public void notifyObservers() {
        MyHandler.sendEmptyMessage(1);
    }

    Handler MyHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if(msg.what == 1) {
                notifyChild();
            }

            return false;
        }
    });


    private void notifyChild() {
        for (int i=0; i<mCountdownObservers.size(); i++) {
            CountdownObserver o = mCountdownObservers.get(i).get();

            if(o != null) {
                Log.d("REECE", "CountdownManager: "+o.getName()+" Call update!");
                o.update();
            } else {
                Log.e("REECE", "CountdownManager: somebody guys is null, cant be update!");
            }
        }
    }
}
