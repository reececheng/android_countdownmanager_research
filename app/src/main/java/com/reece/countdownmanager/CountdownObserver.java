package com.reece.countdownmanager;

/**
 * Created by ReeceCheng on 2016/2/15.
 */
public interface CountdownObserver {
    String getName();
    void update();
}
