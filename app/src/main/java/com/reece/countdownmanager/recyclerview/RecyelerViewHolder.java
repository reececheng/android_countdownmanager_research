package com.reece.countdownmanager.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.reece.countdownmanager.CountTextView;
import com.reece.countdownmanager.R;

/**
 * Created by ReeceCheng on 2016/2/15.
 */
public abstract class RecyelerViewHolder extends RecyclerView.ViewHolder {

    public RecyelerViewHolder(View view) {
        super(view);
    }

    public abstract void bindView(RecyclerData data);


    public static class CountdownViewHolder extends RecyelerViewHolder {

        private CountTextView txt;

        public CountdownViewHolder(View view) {
            super(view);
            txt = (CountTextView) view.findViewById(R.id.countdown_txt);
        }

        @Override
        public void bindView(RecyclerData data) {
            txt.init(data.name, data.time);
        }
    }
}
