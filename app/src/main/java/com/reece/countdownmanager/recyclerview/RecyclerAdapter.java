package com.reece.countdownmanager.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reece.countdownmanager.R;

import java.util.ArrayList;

/**
 * Created by ReeceCheng on 2016/2/15.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyelerViewHolder> {

    private ArrayList<RecyclerData> mData;

    public void setData(ArrayList<RecyclerData> data) {
        mData = data;
    }

    @Override
    public RecyelerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.countdown_layout, parent, false);

        return new RecyelerViewHolder.CountdownViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyelerViewHolder holder, int position) {
        holder.bindView(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
