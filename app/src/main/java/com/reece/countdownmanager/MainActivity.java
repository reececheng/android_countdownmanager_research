package com.reece.countdownmanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.reece.countdownmanager.recyclerview.RecyclerAdapter;
import com.reece.countdownmanager.recyclerview.RecyclerData;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{

    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler);
        mAdapter = new RecyclerAdapter();

        ArrayList<RecyclerData> datas = new ArrayList<>();
        datas.add(new RecyclerData("A1", "2016-02-16 17:10:00"));
        datas.add(new RecyclerData("B1", "2016-02-15 18:11:00"));
        datas.add(new RecyclerData("C1", "2016-02-16 21:30:00"));
        datas.add(new RecyclerData("D1", "2016-02-16 18:00:00"));
        datas.add(new RecyclerData("E1", "2016-02-16 19:00:00"));
        datas.add(new RecyclerData("F1", "2016-02-16 20:30:00"));
        datas.add(new RecyclerData("G1", "2016-02-16 23:00:00"));
        datas.add(new RecyclerData("H1", "2016-02-16 16:30:00"));
        datas.add(new RecyclerData("I1", "2016-02-16 19:00:00"));
        datas.add(new RecyclerData("J1", "2016-02-16 20:00:00"));
        datas.add(new RecyclerData("K1", "2016-02-16 23:30:00"));
        datas.add(new RecyclerData("L1", "2016-02-16 22:00:00"));
        datas.add(new RecyclerData("M1", "2016-02-16 22:00:00"));
        datas.add(new RecyclerData("N1", "2016-02-16 18:30:00"));
        datas.add(new RecyclerData("A2", "2016-02-16 17:10:00"));
        datas.add(new RecyclerData("B2", "2016-02-15 18:11:00"));
        datas.add(new RecyclerData("C2", "2016-02-16 21:30:00"));
        datas.add(new RecyclerData("D2", "2016-02-16 18:00:00"));
        datas.add(new RecyclerData("E2", "2016-02-16 19:00:00"));
        datas.add(new RecyclerData("F2", "2016-02-16 20:30:00"));
        datas.add(new RecyclerData("G2", "2016-02-16 23:00:00"));
        datas.add(new RecyclerData("H2", "2016-02-16 16:30:00"));
        datas.add(new RecyclerData("I2", "2016-02-16 19:00:00"));
        datas.add(new RecyclerData("J2", "2016-02-16 20:00:00"));
        datas.add(new RecyclerData("K2", "2016-02-16 23:30:00"));
        datas.add(new RecyclerData("L2", "2016-02-16 22:00:00"));
        datas.add(new RecyclerData("M2", "2016-02-16 22:00:00"));
        datas.add(new RecyclerData("N2", "2016-02-16 18:30:00"));

        mAdapter.setData(datas);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        CountdownManager.getInstance().toggleCountdown();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        CountdownManager.getInstance().stopCountdown();
    }
}
