package com.reece.countdownmanager;

import java.lang.ref.WeakReference;

/**
 * Created by ReeceCheng on 2016/2/15.
 */
public interface Subject {
    void registerObserver(WeakReference<CountdownObserver> o);
    void removeObserver(WeakReference<CountdownObserver> o);
    void notifyObservers();
}
